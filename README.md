# ITEM AND TRASACTION WITH JWT SECURITY

## Reqiured tools for run this apps
* Mysql
* Java min 8 version
* Postman as API testing tool

## How to use?
1. Create db with your mysql
2. Update db config using your db
3. Run the code
4. Import roles table with this code 
   >INSERT INTO bvk.roles (created,deleted,updated,name) VALUES
   (NULL,NULL,NULL,'ROLE_ADMIN'),
   (NULL,NULL,NULL,'ROLE_USER');
5. Signup user
6. Signin user for get token
7. Congratulation you ready to use this app
8. please import json postman collection for more documentation

## User level
1. Admin
    * Add item 
    * Update item
2. User and Admin
    * Get all item
    * Get item by name
    * Add item to cart
    * Update item in cart (add or delete)
    * Finalize cart
    * Get all cart
    * Get cart by id

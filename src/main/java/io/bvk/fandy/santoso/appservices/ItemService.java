package io.bvk.fandy.santoso.appservices;

import io.bvk.fandy.santoso.entitys.Item;
import io.bvk.fandy.santoso.payloads.response.ItemResponse;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ItemService {
    public List<ItemResponse> mergeItems(List<Item> items) {
        List<ItemResponse> itemResponses = new ArrayList<>();
        for (Item item : items) {
            ItemResponse itemResponse = new ItemResponse();
            itemResponse.setItemName(item.getItemName());
            itemResponse.setId(item.getId());
            itemResponse.setPrice(item.getPrice());
            itemResponses.add(itemResponse);
        }
        return itemResponses;
    }
}

package io.bvk.fandy.santoso.controllers;

import io.bvk.fandy.santoso.appservices.ItemService;
import io.bvk.fandy.santoso.entitys.Item;
import io.bvk.fandy.santoso.payloads.response.ItemResponse;
import io.bvk.fandy.santoso.repositorys.ItemRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/item")
public class ItemController {
    @Autowired
    ItemRepository itemRepository;

    @Autowired
    ItemService itemService;

    @GetMapping({"/", ""})
    public List<ItemResponse> allItem() {
        List<Item> items = itemRepository.findAll();
        List<ItemResponse> itemResponses = new ArrayList<>();
        itemResponses = itemService.mergeItems(items);
        return itemResponses;
    }

    @GetMapping("/item-name")
    public List<ItemResponse> userAccess(@RequestParam(value = "itemName") String itemName) {
        List<Item> items = itemRepository.findByItemNameContaining(itemName);
        List<ItemResponse> itemResponses = new ArrayList<>();
        itemResponses = itemService.mergeItems(items);
        return itemResponses;
    }

    @Transactional(rollbackOn = {SQLException.class, Exception.class, ParseException.class})
    @PostMapping({"/", ""})
    @PreAuthorize("hasRole('ADMIN')")
    public List<Item> save(@RequestBody List<Item> items) {
        items = itemRepository.saveAll(items);
        return items;
    }

    @Transactional(rollbackOn = {SQLException.class, Exception.class, ParseException.class})
    @PutMapping({"/", ""})
    @PreAuthorize("hasRole('ADMIN')")
    public List<Item> update(@RequestBody List<Item> items) {
        items = itemRepository.saveAll(items);
        return items;
    }
}

package io.bvk.fandy.santoso.controllers;

import io.bvk.fandy.santoso.entitys.Cart;
import io.bvk.fandy.santoso.entitys.Item;
import io.bvk.fandy.santoso.repositorys.CartRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.*;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/transaction")
public class TransactionController {
    @Autowired
    CartRepository cartRepository;

    @GetMapping("/")
    @PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
    public Map allTransaction() {
        Map result = new HashMap();
        String message = "";
        List<Cart> carts = new ArrayList<>();
        carts = cartRepository.findAll();
        result.put("message", message);
        result.put("cart", carts);
        return result;
    }

    @GetMapping("/id")
    @PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
    public Map transactionById(Long cartId) {
        Map result = new HashMap();
        String message = "";
        Cart cartFromDB = new Cart();
        cartFromDB = cartRepository.findById(cartId).orElse(null);
        if (null == cartFromDB) {
            message = "cart not found.";
            result.put("message", message);
            return result;
        }
        result.put("message", message);
        result.put("cart", cartFromDB);
        return result;
    }

    @PostMapping("/")
    @PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
    public Map addToCart(@RequestBody List<Item> items) {
        Map result = new HashMap();
        String message = "";
        Cart cart = new Cart();
        cart.setFinish(Boolean.FALSE);
        Set<Item> itemSet = new HashSet<>();
        for (Item item : items) {
            itemSet.add(item);
        }
        cart.setItems(itemSet);
        cart = cartRepository.save(cart);
        message = "success.";
        result.put("message", message);
        result.put("cart", cart);
        return result;
    }

    @PutMapping("/")
    @PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
    public Map updateToCart(@RequestBody Cart cart) {
        Map result = new HashMap();
        String message = "";
        Cart cartFromDB = cartRepository.findById(cart.getId()).orElse(null);
        if (null == cartFromDB) {
            message = "cart not found.";
            result.put("message", message);
            return result;
        }
        if (cartFromDB.getFinish() == Boolean.TRUE) {
            message = "cart has been finished.";
            result.put("message", message);
            return result;
        }
        cart = cartRepository.save(cart);
        message = "success.";
        result.put("message", message);
        result.put("cart", cart);
        return result;
    }

    @PostMapping("/finish")
    @PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
    public Map finishCart(@RequestBody Cart cart) {
        Map result = new HashMap();
        String message = "";
        cart.setFinish(Boolean.TRUE);
        BigDecimal totalPrice = BigDecimal.ZERO;
        for (Item item : cart.getItems()) {
            totalPrice = totalPrice.add(item.getPrice());
        }
        cart.setTotalItemPrice(totalPrice);
        cart = cartRepository.save(cart);
        message = "success.";
        result.put("message", message);
        result.put("cart", cart);
        return result;
    }
}

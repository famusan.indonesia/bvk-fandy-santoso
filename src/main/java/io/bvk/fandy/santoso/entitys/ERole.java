package io.bvk.fandy.santoso.entitys;

public enum ERole {
  ROLE_USER,
  ROLE_ADMIN
}

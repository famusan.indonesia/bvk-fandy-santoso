package io.bvk.fandy.santoso.payloads.response;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

@Setter
@Getter
public class ItemResponse {
    private Long id;
    private String itemName;
    private BigDecimal price;
}

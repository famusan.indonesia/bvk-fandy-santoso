package io.bvk.fandy.santoso.repositorys;

import io.bvk.fandy.santoso.entitys.Cart;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CartRepository extends JpaRepository<Cart, Long> {
}

package io.bvk.fandy.santoso.repositorys;

import io.bvk.fandy.santoso.entitys.Item;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ItemRepository extends JpaRepository<Item, Long> {

    List<Item> findByItemNameContaining(String itemName);
}

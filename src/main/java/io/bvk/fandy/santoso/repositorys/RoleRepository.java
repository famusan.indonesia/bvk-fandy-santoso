package io.bvk.fandy.santoso.repositorys;

import io.bvk.fandy.santoso.entitys.ERole;
import io.bvk.fandy.santoso.entitys.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface RoleRepository extends JpaRepository<Role, Long> {
  Optional<Role> findByName(ERole name);
}
